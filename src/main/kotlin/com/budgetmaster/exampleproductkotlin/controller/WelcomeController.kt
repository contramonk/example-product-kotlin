package com.budgetmaster.exampleproductkotlin.controller

import com.budgetmaster.exampleproductkotlin.model.Person
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class WelcomeController {

    @GetMapping("/welcome")
    fun welcome(
        @RequestParam firstName: String,
        @RequestParam lastName: String,
        @RequestParam age: Int
    ): ResponseEntity<Person> {

        val person = Person(firstName, lastName, age)

        return ResponseEntity.ok(person)
    }
}
