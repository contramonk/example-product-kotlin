package com.budgetmaster.exampleproductkotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ExampleProductKotlinApplication

fun main(args: Array<String>) {
    runApplication<ExampleProductKotlinApplication>(*args)
}
