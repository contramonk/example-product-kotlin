package com.budgetmaster.exampleproductkotlin.model

data class Person(val firstName: String, val lastName: String, val age: Int)
